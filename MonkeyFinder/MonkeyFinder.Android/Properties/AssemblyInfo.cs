﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General info through attributes
[assembly: AssemblyTitle("MonkeyFinder.iOS")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("MonkeyFinder.iOS")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Makes ComVisible not visible to COM components.
[assembly: ComVisible(false)]

[assembly: Guid("72bdc44f-c588-44f3-b6df-9aace7daafdd")]

[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.*")]