﻿using MonkeyFinder.View;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace MonkeyFinder
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //setting the startup application to a new nav page
            //gives a toolbar on top
            //wrapping the main page
            MainPage = new NavigationPage(new MainPage())
            {
                BarBackgroundColor = (Color)Resources["Primary"],
                BarTextColor = Color.White
            };
        }

        protected override void OnStart()
        {
            //Used for when the app starts
        }

        protected override void OnSleep()
        {
            //Used when the app is sleeping
        }

        protected override void OnResume()
        {
            //Use when the app starts up again
        }
    }
}
